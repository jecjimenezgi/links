import { Redirect, useParams } from "react-router-dom";
import { Switch } from "react-router-dom";

const Post = ({ obj }) => {
    const params = useParams();
  if (obj) {


    let item = obj.filter(
      (item) => item.id.toString() === params.id.toString()
    )[0];

    return (
      <div>
        <h2>{item.title}</h2>
        <p>{item.content}</p>
        <p>{item.author}</p>
        <p>{item.publishedAt}</p>
      </div>
    );
  } else {
    return (
        <Switch>
          <Redirect to="/posts"></Redirect>
        </Switch>
    );
  }
};

export default Post;

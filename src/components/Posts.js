import {useRouteMatch,Route, Switch, Link} from 'react-router-dom'
import Post from './Post'
import { get_benders } from '../services/get.js'
import { useState, useEffect } from 'react'


 
const Posts = () => {
    const {path} = useRouteMatch()
    const [navPost, setNavPost] = useState(null)
    const [obj, setObj] = useState(null)

    useEffect(() => {
        get_benders().then(response => response.json())
        .then(result => {
            setObj(result)
          let list = result.map( obj => {
            return (
              <li key={obj.id}><Link to={`${path}/${obj.id}`}>{obj.title}</Link></li>
            )
          })
          setNavPost(list)

        })
        .catch(error => console.log('error', error));

    },[path])

    return (
      <div>
        <h2>Haz click sobre una de las opciones para ver mas información</h2>
        <ul>
        {navPost}
        </ul>
        <Switch>
          <Route path={`${path}/:id`}>
            <Post obj={obj}/>
          </Route>
        </Switch>
      </div>
    );
  }
  
  export default Posts;
import './App.css';
import {HashRouter as Router, Switch, Route, Redirect} from "react-router-dom"
import Home from "./components/Home.js";
import Posts from "./components/Posts.js";
import Navbar from "./components/Navbar.js";

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar></Navbar>
        <Switch>
        <Route path="/home">
            <Home />
          </Route>
          <Route path="/posts">
            <Posts />
          </Route>
          <Route>
            <Redirect to="/home" />
          </Route>
        </Switch> 
      </Router>
    </div>
  );
}

export default App;
